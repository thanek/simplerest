<?php
namespace xis\Controller;

use Symfony\Component\HttpFoundation\Request;

class HelloController
{
    public function indexAction(Request $request)
    {
        return array('text' => 'Hello!');
    }
} 