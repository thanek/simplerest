<?php
require_once 'vendor/autoload.php';

use Nocarrier\Hal;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$request = Request::createFromGlobals();
$routes = new RouteCollection();
$routes->add('hello', new Route('/hello/{name}',
        array('_controller' => 'xis\Controller\HelloController::indexAction'))
);

$context = new RequestContext();
$context->fromRequest($request);
$matcher = new UrlMatcher($routes, $context);
$resolver = new ControllerResolver();

try {
    $parameters = $matcher->match($request->getPathInfo());
    $request->attributes->add($parameters);

    $controller = $resolver->getController($request);
    $arguments = $resolver->getArguments($request, $controller);

    $code = 200;
    $returnValue = call_user_func_array($controller, $arguments);
    $responseContent = new Hal($routes->get($parameters['_route'])->getPath(), $returnValue);
    $responseContent = $responseContent->asJson(true);
} catch (ResourceNotFoundException $e) {
    $code = 404;
    $responseContent = 'Not found';
} catch (Exception $e) {
    $code = 500;
    $responseContent = 'An error occurred: ' . $e->getMessage();
}

$response = new Response($responseContent, $code);
$response->send();